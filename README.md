# *Currículum Prova*


![foto](Kanjar.png)


**Nom:** Ahmer Muhammad

**Adreça:** c/Joan de peguera num 98

**Telèfon:** 731 256 356

**Mail:** x7984018m@iespoblenou.org

**Web:** [LaMevaWeb](https://www.w3schools.com/html/)


## Actualment

**Estudiant:** cursant 2n SMIX

**Treballant:** Universitat de Barcelona

## Estudis

**ESO:** A l'INS Joan manuel zafra *excelent*


## Idiomes


Idioma | comprensió oral | comprensió escrita | expressió oral | expressió escrita 
---|---|---|---|---
Català | molt bo | molt bo | molt bo | molt bo
Castellà | molt bo | molt bo | molt bo | molt bo
Angles |bo |  bo |  bo |  bo
Alemany | molt bo | molt bo | bo | bo


## Hobbies

> M'agrada jugar a videojocs i segueixo ufc.

## Capacitats transversals

- Autònom
- Sociable
- Treballador
- Responsable

## Futur i aspiracions

M'agradaria continuar els meus estudis:
- [x] Treure'm el 1r de SMIX.
- [x] Realitzar el cicle formatiu de grau superior de DAM.
- [x] Treure'm el carnet de conduir
- [x] Obtenir un certificat d'anglès
    

## Codi que m'agrada

```java
public class HelloWorld {

    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");


